import threading
import time
import paho.mqtt.client as mqtt
import requests


class publish_switches(threading.Thread):
    def __init__(self, threadID, name, counter):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.counter = counter
    def run(self):
        print "Starting " + self.name
        #print_time(self.name, self.counter, 5)
        #print "Sending.."
        

        while True:
           time.sleep(2)
           for i in range(0, len(switches)):
               
                print "Collect from API"
                site = 'https://iot.bsad.dk/api/getValue?id={}'.format(switches[i])
                r = requests.get(site, verify=True)
                print r.text
                print switches2.get(switches[i])
                if (r.text == switches2.get(switches[i])):
                    print "no update"
                else:
                    print "update"
                    switches2.update({switches[i]:r.text})

                    mqttc = mqtt.Client("python_pub")
                    mqttc.connect("localhost", 1883)
                    topic = 'iot/switch/{}'.format(switches[i])
                
             
                    msg = r.text

                    mqttc.publish(topic, msg)
                    print "publish mqtt"
           

        print "Exiting " + self.name




class subscribe_temps(threading.Thread):
    def __init__(self, threadID, name, counter):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.counter = counter
    def run(self):
        print "Starting " + self.name
        client = mqtt.Client()
        client.on_connect = on_connect
        client.on_message = on_message
        client.connect("localhost", 1883, 60)
        client.loop_forever()
        #while True:

            #time.sleep(10)

        print "Exiting " + self.name




# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, rc):
    #print("Connected with result code "+str(rc))
    client.subscribe("iot/temp/#")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print "Topic: ", msg.topic+'\nMessage: '+str(msg.payload)
    hw_id = msg.topic[9:15]
    value = msg.payload
    print hw_id
    print value
    payload = {'id': hw_id, 'value': value}
    requests.put("https://iot.bsad.dk/api/gatewayUpdate", data=payload, verify=True)

    print "Connecting API"




switches2 = {'467274': 0}

switches = ['467274']
temps = ['444444']

try:
  # Create new threads
  thread1 = publish_switches(1, "publish_switches", 1)
  thread2 = subscribe_temps(1, "subscribe_temps", 1)

  thread1.daemon=True
  thread2.daemon=True


  # Start new Threads
  thread1.start()
  thread2.start()

  while True: time.sleep(100)
except (KeyboardInterrupt, SystemExit):
  print '\n! Received keyboard interrupt, quitting threads.\n'






